//import path from 'path';
var path = require('path');

const express = require("express");
//const bodyParser = require("body-parser");
var i18n = require("i18n");
i18n.configure({
	locales:['en', 'vi', 'jp'],
	directory: __dirname + '/locales',
	cookie: 'lang',
});

const config = require('./config/appConfig');

var app = express();
// parse requests of content-type - application/json
app.use(express.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'static')));
app.use(i18n.init);
app.set('views', path.join(__dirname, './views'));
app.set('view engine', 'ejs')

app.get('/', function (req, res) {
  	res.render('index', {});
});

require("./routes/authRoutes.js")(app);
require("./routes/customerRoutes.js")(app); 

app.listen(config.app.port, function () {
  	console.log('Example app listening on port '+config.app.port);
});