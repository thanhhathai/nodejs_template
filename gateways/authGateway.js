const i18n = require('i18n')
const Logger = require('../helpers/logger');
const RequestHandler = require('../helpers/requestHandler');
const logger = new Logger();
const requestHandler = new RequestHandler(logger);

//const jwtVariable = require('../../variables/jwt');
const randToken = require('rand-token');
const bcrypt = require('bcrypt');
const userModel = require('../models/user');
const authHelper = require('../helpers/authHelpers');
const config = require('../config/appConfig');

exports.registerResponse = async (username, password) => {
    if (!username || !password) {
        return requestHandler.sendError(
            i18n.__('Missing params')
        )
    }
	username = username.toLowerCase();
    const user = await userModel.getUser(username);
	if (user) {
        return requestHandler.sendError(i18n.__('This account has already existed.'), {}, 409);
    }
	else {
		const hashPassword = bcrypt.hashSync(password, config.auth.saltRounds);
		const newUser = ({
			username: username,
			password: hashPassword
		});
        const createUser = await userModel.createUser(newUser);
		if (!createUser) {
            return requestHandler.sendError(i18n.__("There was an error creating the account, please try again."), {}, 400);
		}
		return requestHandler.sendSuccess({username,});
	}
}

exports.loginResponse = async (username, password) => {
    if (!username || !password) {
        return requestHandler.sendError(
            i18n.__('Missing params')
        )
    }
    username = username.toLowerCase();
    const user = await userModel.getUser(username);
	if (!user) {
        return requestHandler.sendError(i18n.__('Username does not exist.'), {}, 401);
	}
	const isPasswordValid = bcrypt.compareSync(password, user.password);
	if (!isPasswordValid) {
        return requestHandler.sendError(i18n.__('Incorrect password.'), {}, 401);
	}

	const accessTokenLife = config.auth.jwt_expiresin;
	const accessTokenSecret = config.auth.jwt_secret;

	const dataForAccessToken = {
		username: user.username,
	};
	const accessToken = await authHelper.generateToken(
		dataForAccessToken,
		accessTokenSecret,
		accessTokenLife,
	);
	if (!accessToken) {
        return requestHandler.sendError(i18n.__('Login failed, please try again.'), {}, 401);
	}

	let refreshToken = randToken.generate(config.auth.refresh_token_size); // tạo 1 refresh token ngẫu nhiên
	if (!user.refresh_token) {
		// Nếu user này chưa có refresh token thì lưu refresh token đó vào database
		await userModel.updateRefreshToken(user.username, refreshToken);
	} else {
		// Nếu user này đã có refresh token thì lấy refresh token đó từ database
		refreshToken = user.refresh_token;
	}

	return requestHandler.sendSuccess({
		message: i18n.__("Logged in successfully."),
		accessToken,
		refreshToken,
		user,
	});
}

exports.refreshTokenResponse = async (accessTokenFromHeader, refreshTokenFromBody) => {
	if (!accessTokenFromHeader) {
        return requestHandler.sendError(i18n.__('Cannot find access token.'), {}, 400);
	}

	if (!refreshTokenFromBody) {
        return requestHandler.sendError(i18n.__('No refresh token found.'), {}, 400);
	}

	const accessTokenSecret = config.auth.jwt_secret;
	const accessTokenLife = config.auth.jwt_expiresin;

	// Decode access token đó
	const decoded = await authHelper.decodeToken(
		accessTokenFromHeader,
		accessTokenSecret,
	);
	if (!decoded) {
        return requestHandler.sendError(i18n.__('Invalid Access token.'), {}, 400);
	}

	const username = decoded.payload.username; // Lấy username từ payload

	const user = await userModel.getUser(username);
	if (!user) {
        return requestHandler.sendError(i18n.__('User does not exist.'), {}, 401);
	}

	if (refreshTokenFromBody !== user.refresh_token) {
        return requestHandler.sendError(i18n.__('Invalid token refresh.'), {}, 400);
	}

	// Tạo access token mới
	const dataForAccessToken = {
		username,
	};

	const accessToken = await authHelper.generateToken(
		dataForAccessToken,
		accessTokenSecret,
		accessTokenLife,
	);
	if (!accessToken) {
        return requestHandler.sendError(i18n.__('Access token creation failed, please try again.'), {}, 400);
	}
	return requestHandler.sendSuccess({
		accessToken,
	});
}