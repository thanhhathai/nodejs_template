const i18n = require('i18n')
const Logger = require('../helpers/logger');
const RequestHandler = require('../helpers/requestHandler');
const logger = new Logger();
const requestHandler = new RequestHandler(logger);

const Customer = require("../models/customer.js");

exports.createResponse = async (name, email, active) => {
    // Validate request
    if (!name || !email || !active) {
        return requestHandler.sendError(
            i18n.__('Missing params')
        )
    }

    // Create a Customer
    const customer = new Customer({
        email: email,
        name: name,
        active: active
    });

    // Save Customer in the database
    try {
        let res = await Customer.create(customer);
        return requestHandler.sendSuccess(res);
    } catch( err) {
        return requestHandler.sendError(err.sqlMessage);
    }
}

exports.findAllResponse = async () => {
    try {
        let res = await Customer.getAll();
        return requestHandler.sendSuccess(res);
    } catch(error) {
        return requestHandler.sendError(err.sqlMessage);
    }
}

exports.findByIdResponse = async (customerId) => {
    try {
        let res = await Customer.findById(customerId);
        return requestHandler.sendSuccess(res);
    } catch(error) {
        return requestHandler.sendError(err.sqlMessage);
    }
}

exports.updateByIdResponse = async (customerId, dataUpdate) => {
    // Validate Request
    if (!dataUpdate || !customerId) {
        return requestHandler.sendError(
            i18n.__('Missing params')
        )
    }

    try {
        let res = await Customer.updateById(customerId, new Customer(dataUpdate));
        return requestHandler.sendSuccess(res);
    } catch(error) {
        return requestHandler.sendError(err.sqlMessage);
    }
}

exports.removeResponse = async (customerId) => {
    try {
        let res = await Customer.remove(customerId);
        return requestHandler.sendSuccess(res);
    } catch(error) {
        return requestHandler.sendError(err.sqlMessage);
    }
}

exports.removeAllResponse = async () => {
    try {
        let res = await Customer.removeAll();
        return requestHandler.sendSuccess(res);
    } catch(error) {
        return requestHandler.sendError(err.sqlMessage);
    }
}