module.exports = app => {
	const auth = require("../controllers/authController.js");

	// 
	app.post("/auth/register", auth.register);
	app.post('/auth/login', auth.login);
	app.post('/auth/refresh', auth.refreshToken);
};