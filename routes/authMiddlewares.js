const config = require('../config/appConfig');

const userModle = require('../models/user');

const authHelper = require('../helpers/authHelpers');

exports.isAuth = async (req, res, next) => {
	// Lấy access token từ header
	const accessTokenFromHeader = req.headers.x_authorization;
	if (!accessTokenFromHeader) {
		return res.status(401).send('Không tìm thấy access token!');
	}

	const accessTokenSecret = config.auth.jwt_secret;

	const verified = await authHelper.verifyToken(
		accessTokenFromHeader,
		accessTokenSecret,
	);
	if (!verified) {
		return res
			.status(401)
			.send('Bạn không có quyền truy cập vào tính năng này!');
	}

	const user = await userModle.getUser(verified.payload.username);
	req.user = user;

	return next();
};