module.exports = app => {
	const customers = require("../controllers/customersController.js");
	const authMiddleware = require('./authMiddlewares');

	const isAuth = authMiddleware.isAuth;
	// Create a new Customer
	app.post("/customers", customers.create);

	// Retrieve all Customers
	app.get("/customers", isAuth, customers.findAll);

	// Retrieve a single Customer with customerId
	app.get("/customers/:customerId", customers.findOne);

	// Update a Customer with customerId
	app.put("/customers/:customerId", customers.update);

	// Delete a Customer with customerId
	app.delete("/customers/:customerId", customers.delete);

	// Delete all Customers
	app.delete("/customers", customers.deleteAll);
};