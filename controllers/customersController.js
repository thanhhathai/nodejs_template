const CustomerGateway = require("../gateways/customerGateway");
//const { response } = require("express");

// Create and Save a new Customer
exports.create = async (req, res) => {
	let name = req.body.name;
	let email = req.body.email;
	let active = req.body.active;
	let {status, dataResponse} = await CustomerGateway.createResponse(name, email, active);
	res.status(status).send(dataResponse);
};

// Retrieve all Customers from the database.
exports.findAll = async (req, res) => {
	let {status, dataResponse} = await CustomerGateway.findAllResponse();
	res.status(status).send(dataResponse);
};

// Find a single Customer with a customerId
exports.findOne = async (req, res) => {
	let customerId = req.params.customerId;
	let {status, dataResponse} = await CustomerGateway.findByIdResponse(customerId);
	res.status(status).send(dataResponse);
};

// Update a Customer identified by the customerId in the request
exports.update = async (req, res) => {
	let customerId = req.params.customerId;
	let dataUpdate = req.body;
	let {status, dataResponse} = await CustomerGateway.updateByIdResponse(customerId, dataUpdate);
	res.status(status).send(dataResponse);
};

// Delete a Customer with the specified customerId in the request
exports.delete = async (req, res) => {
	let customerId = req.params.customerId;
	let {status, dataResponse} = await CustomerGateway.removeResponse(customerId);
	res.status(status).send(dataResponse);
};

// Delete all Customers from the database.
exports.deleteAll = async (req, res) => {
	let {status, dataResponse} = await CustomerGateway.removeAllResponse();
	res.status(status).send(dataResponse);
};