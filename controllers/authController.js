const authGateway = require('../gateways/authGateway');

exports.register = async (req, res) => {
	let username = req.body.username;
	let password = req.body.password;
	let {status, dataResponse} = await authGateway.registerResponse(username, password);
	res.status(status).send(dataResponse);
};

exports.login = async (req, res) => {
	let username = req.body.username;
	let password = req.body.password;
	let {status, dataResponse} = await authGateway.loginResponse(username, password);
	res.status(status).send(dataResponse);
};

exports.refreshToken = async (req, res) => {
	const accessTokenFromHeader = req.headers.x_authorization;
	const refreshTokenFromBody = req.body.refresh_token;
	let {status, dataResponse} = await authGateway.refreshTokenResponse(accessTokenFromHeader, refreshTokenFromBody);
	res.status(status).send(dataResponse);
};