const sql = require("../config/db.js");

// constructor
const Customer = function(customer) {
	this.email = customer.email;
	this.name = customer.name;
	this.active = customer.active;
};

Customer.create = (newCustomer) => {
	return new Promise((resolve, reject)=>{
		sql.query("INSERT INTO customers SET ?", newCustomer, (err, res) => {
			if (err) {
				//console.log("error: ", err);
				return reject(err);
			}
			//console.log("created customer: ", { id: res.insertId, ...newCustomer });
			return resolve({ id: res.insertId, ...newCustomer });
		});
	})
  
};

Customer.findById = (customerId) => {
	return new Promise((resolve, reject)=>{
		sql.query(`SELECT * FROM customers WHERE id = ${customerId}`, (err, res) => {
			if (err) {
				return reject(err);
			}
			return resolve(res);
		});
	})
};

Customer.getAll = () => {
	return new Promise((resolve, reject)=>{
		sql.query("SELECT * FROM customers", (err, res) => {
			if (err) {
				return reject(err);
			}
			return resolve(res);
		});
	})
 
};

Customer.updateById = (id, customer) => {
	return new Promise((resolve, reject)=>{
		sql.query("UPDATE customers SET email = ?, name = ?, active = ? WHERE id = ?",
			[customer.email, customer.name, customer.active, id],
			(err, res) => {
				if (err) {
					return reject(err);
				}
		
				if (res.affectedRows == 0) {
					// not found Customer with the id
					return resolve({ message: "not found" });
				}
			  	return resolve({ id: id, ...customer });
			}
		);
	})
};

Customer.remove = (id) => {
	return new Promise((resolve, reject)=>{
		sql.query("DELETE FROM customers WHERE id = ?", id, (err, res) => {
			if (err) {
				return reject(err);
			}
		
			if (res.affectedRows == 0) {
			  	// not found Customer with the id
			  	return resolve({ message: "not found" });
			}
			return resolve({message: true});
		  });
	})
  
};

Customer.removeAll = () => {
	return new Promise((resolve, reject)=>{
		sql.query("DELETE FROM customers", (err, res) => {
			if (err) {
				return reject(err);
			}
			return resolve({message: true});
		});
	})
};

module.exports = Customer;