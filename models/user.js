const sql = require("../config/db.js");

// constructor
const User = function(user) {
    this.username = user.username;
    this.password = user.password;
};

User.getUser = username => {
    return new Promise((resolve, reject)=>{
        const data = sql.query(`SELECT * FROM users WHERE username = "${username}"`,  (error, elements)=>{
            if(error){
                return reject(error);
            }
            return resolve(elements[0]);
        });
	})
};

User.createUser = newUser => {
    return new Promise((resolve, reject)=>{
        sql.query("INSERT INTO users SET ?", newUser,  (error, elements)=>{
            if(error){
                return reject(error);
            }
            return resolve(elements);
        })
    })
};

User.updateRefreshToken = (username, refreshToken) => {
    return new Promise((resolve, reject)=>{
        sql.query("UPDATE users SET refresh_token = ? WHERE username = ?",[refreshToken, username],
        (error, elements)=>{
            if(error){
                return reject(error);
            }
            return resolve(elements);
        });
    })
};

module.exports = User;